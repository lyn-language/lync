use crate::types::Atom;
use std::collections::BTreeMap;

/// An environment is a mapping from variable names to their values and provides methods to
/// resolve names and add entries.
pub struct Environment {
	data: BTreeMap<String, Atom>
}

impl Environment {
	pub fn new() -> Self {
		let data = BTreeMap::new();

		Self {
			data
		}
	}

	/// Create a new variable entry in the environment
	pub fn define(&mut self, key: String, value: Atom) {
		self.data.insert(key, value);
	}

	/// Remove the variable with the given name. The returned boolean indicates if
	/// there was a binding to remove.
	pub fn drop(&mut self, key: String) -> bool {
		match self.data.remove(&key) {
			Some(_) => true,
			None 	=> false
		}
	}

	/// Find and get the value of a binding with the given name
	pub fn resolve(&mut self, key: String) -> Result<&Atom, String> {
		match self.data.get(&key) {
	        Some(atom) => Ok(atom),
	        None 	   => Err(format!("Cannot resolve the name {:?} in this scope", key)),
	    }
	}
}