pub mod environment;

use crate::types::{Atom, Literal};
pub use self::environment::Environment;

pub struct Interpreter {
	environment: Environment,
}

impl Interpreter {
	pub fn new() -> Self {
		Self {
			environment: Environment::new()
		}
	}

	fn resolve(&mut self, name: String) -> Result<Atom, String> {
		match self.environment.resolve(name) {
			Ok(atom) => Ok(atom.clone()),
			Err(e)       => Err(e)
		}
	}

	pub fn evaluate(&mut self, atom: Atom) -> Result<Atom, String> {
		match atom {
			_ => Ok(atom)
		}
	}

	fn call(&mut self, expr: &'static mut Atom, args: Vec<Atom>) -> Result<Atom, String> {
	
	}
}