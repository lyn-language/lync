use crate::types::{
	Atom,
	Molecule,
};

pub struct Moleculizer {
	atoms: Vec<Atom>,
	position: usize,
}

impl Moleculizer {
	pub fn new() -> Self {
		Self {
			atoms: vec![],
			position: 0,
		}
	}

	pub fn process(&mut self, source: Vec<Atom>) -> Result<Vec<Molecule>, String> {
		let mut molecules = vec![];

		self.atoms = source;
		self.position = 0;

		while !self.eof() {
			let atom = self.peek();

			match atom {
				Atom::List { children, is_literal } => {
					molecules.push(self.moleculize_list());
				},

				_ => {}
			}
		}

		Ok(molecules)
	}

	fn moleculize_list(children: Vec<Atom>, is_literal: bool) -> Result<Molecule, String> {
		if !is_literal {
			match &children[0] {
				Atom::Symbol { name } => {}
			}
		}
	}

	fn expect(&mut self, atom: Atom) -> bool {
		&atom == self.peek()
	}

	fn expect_and_consume(&mut self, atom: Atom) -> bool {
		if &atom == self.peek() {
			self.consume();
			true
		} else {
			false
		}
	}

	fn peek(&self) -> &Atom {
		&self.atoms[self.position]
	}

	fn consume(&mut self) {
		if !self.eof() {
			self.position += 1;
		}
	}

	fn eof(&self) -> bool {
		self.position == self.atoms.len()
	}
}