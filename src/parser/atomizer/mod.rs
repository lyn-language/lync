use crate::types::{
	Atom,
	Literal,
	Item,
};

use pest::{Parser};
use pest::iterators::Pair;

#[cfg(test)] 
mod tests;

#[derive(Parser)]
#[grammar = "parser/atomizer/grammar.pest"] // relative to /src
pub struct Atomizer;

/// Parse a string of Lyn source code into a collection of [Atom]s
pub fn atomize(source: String) -> Result<Vec<Atom>, String> {
	let mut result = vec![];

	match Atomizer::parse(Rule::Program, &source) {
		Ok(atoms) => {
			for atom in atoms {
				result.push(self::parse_atom(atom));
			}
		},

		Err(e) => return Err(e.to_string())
	}

	Ok(result)
}

fn parse_atom(atom: Pair<self::Rule>) -> Atom {
	let mut children = vec![];
	let mut is_literal = false;

	// Iterate over the children of the Atom
	for inner_pair in atom.clone().into_inner() {
		match inner_pair.as_rule() {
			Rule::LiteralMarker => { is_literal = true },

			_ => children.push(self::parse_atom(inner_pair))
		}
	}

	// Atom = _{ List | Type | String | Boolean | Number | Comment | Variable | Block }

	match atom.as_rule() {
		Rule::List    => {
			if is_literal {
				Atom::Literal(Literal::List {
					children: children.iter().map(|atom| Item::Atom(atom.clone())).collect()
				})
			} else {
				Atom::List {
					children
				}
			}
		},

		Rule::Type    => Atom::Type {
			name: atom.as_str().to_string()
		},

		Rule::String  => self::parse_string(atom),
		Rule::Number  => self::parse_number(atom),
		Rule::Boolean => self::parse_boolean(atom),

		Rule::Variable  => {
			let mut name = "".to_string();
			let mut type_id = None;

			for inner_pair in atom.clone().into_inner() {
				match inner_pair.as_rule() {
					Rule::Symbol => { 
						name = inner_pair.as_str().to_string() 
					},

					Rule::Type => {
						type_id = Some(inner_pair.as_str().to_string())
					},

					_ => children.push(self::parse_atom(inner_pair))
				}
			}

			Atom::Symbol { 
				name,
				type_id,
			}
		},

		Rule::Block => Atom::Block {
			children,
		},

		Rule::Comment => Atom::Comment {
			content: atom.as_str().to_string(),
		},

		_ => Atom::Comment {
			content: "unreachable code".to_string(),
		}
	}
}

fn parse_string(pair: Pair<self::Rule>) -> Atom {
	let mut string = pair.as_str().to_string();

	// Remove the quotes from the string value
	string.remove(0);
	string.remove(string.len() - 1);

	Atom::Literal(
		Literal::String {
			value: string
		}
	)
}

fn parse_boolean(pair: Pair<self::Rule>) -> Atom {
	match pair.as_str() {
		"true" => Atom::Literal(
			Literal::Boolean {
				value: true
			}
		),

		"false" => Atom::Literal(
			Literal::Boolean {
				value: false
			}
		),

		_ => unreachable!()
	}
}

fn parse_number(pair: Pair<self::Rule>) -> Atom {
	let number_str = pair.as_str();

	if number_str.contains(".") {
		Atom::Literal(Literal::Float { 
			value: number_str.to_string().parse::<f64>().unwrap(),
		})
	} else if number_str.contains(",") {
		let modified_str = number_str.replace(",", ".");

		Atom::Literal(Literal::Float { 
			value: modified_str.to_string().parse::<f64>().unwrap()
		})
	} else {
		Atom::Literal(Literal::Integer { 
			value: number_str.to_string().parse::<i64>().unwrap()
		})
	}
}