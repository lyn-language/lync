use super::*;

#[test] fn parse_type() {
	assert_eq!(
		atomize("Lyn".to_string()).unwrap(),
		vec![
			Atom::Type {
				name: "Lyn".to_string()
			}
		]
	)
}

#[test] fn parse_number() {
	assert_eq!(
		atomize("435683".to_string()).unwrap(),
		vec![
			Atom::Literal(
				Literal::Integer {
					value: 435683
				}
			)
		]
	);

	assert_eq!(
		atomize("3.141592653".to_string()).unwrap(),
		vec![
			Atom::Literal(
				Literal::Float {
					value: 3.141592653
				}
			)
		]
	);

	assert_eq!(
		atomize("52,3786".to_string()).unwrap(),
		vec![
			Atom::Literal(
				Literal::Float {
					value: 52.3786
				}
			)
		]
	);
}

#[test] fn parse_boolean() {
	assert_eq!(
		atomize("true".to_string()).unwrap(),
		vec![
			Atom::Literal(
				Literal::Boolean {
					value: true
				}
			)
		]
	);

	assert_eq!(
		atomize("false".to_string()).unwrap(),
		vec![
			Atom::Literal(
				Literal::Boolean {
					value: false
				}
			)
		]
	);
}

#[test] fn parse_variable() {
	assert_eq!(
		atomize("Δv".to_string()).unwrap(),
		vec![
			Atom::Symbol {
				name: "Δv".to_string(),
				type_id: None,
			}
		]
	);

	assert_eq!(
		atomize("x::Int".to_string()).unwrap(),
		vec![
			Atom::Symbol {
				name: "x".to_string(),
				type_id: Some("Int".to_string()),
			}
		]
	);
}

#[test] fn parse_string() {
	assert_eq!(
		atomize("\"Hello, World!\"".to_string()).unwrap(),
		vec![
			Atom::Literal(
				Literal::String {
					value: "Hello, World!".to_string(),
				}
			)
		]
	);

	assert_eq!(
		atomize("'Hello, World!'".to_string()).unwrap(),
		vec![
			Atom::Literal(
				Literal::String {
					value: "Hello, World!".to_string(),
				}
			)
		]
	)
}

#[test] fn parse_list() {
	assert_eq!(
		atomize("(+ 1 2)".to_string()).unwrap(),
		vec![
			Atom::List {
				children: vec![
					Atom::Symbol {
						name: "+".to_string(),
						type_id: None,
					},

					Atom::Literal(
						Literal::Integer {
							value: 1,
						}
					),

					Atom::Literal(
						Literal::Integer {
							value: 2,
						}
					),
				],
			}
		]
	)
}

#[test] fn parse_list_recursively() {
	assert_eq!(
		atomize("(+ (* 1 2) (* 3 4))".to_string()).unwrap(),
		vec![
			Atom::List {
				children: vec![
					Atom::Symbol {
						name: "+".to_string(),
						type_id: None,
					},

					Atom::List {
						children: vec![
							Atom::Symbol {
								name: "*".to_string(),
								type_id: None,
							},

							Atom::Literal(
								Literal::Integer {
									value: 1,
								}
							),

							Atom::Literal(
								Literal::Integer {
									value: 2,
								}
							),
						],
					},

					Atom::List {
						children: vec![
							Atom::Symbol {
								name: "*".to_string(),
								type_id: None,
							},

							Atom::Literal(
								Literal::Integer {
									value: 3,
								}
							),

							Atom::Literal(
								Literal::Integer {
									value: 4,
								}
							),
						],
					},
				],
			}
		]
	)
}

#[test] fn parse_literal_list() {
	assert_eq!(
		atomize("(add '(1 2))".to_string()).unwrap(),
		vec![
			Atom::List {
				children: vec![
					Atom::Symbol {
						name: "add".to_string(),
						type_id: None,
					},

					Atom::Literal(Literal::List {
						children: vec![
							Item::Atom(
								Atom::Literal(Literal::Integer {
									value: 1
								})
							),

							Item::Atom(
								Atom::Literal(Literal::Integer {
									value: 2
								})
							),
						],
					})
				],
			}
		]
	)
}

#[test] fn parse_block() {
	assert_eq!(
		atomize("{ }".to_string()).unwrap(),
		vec![
			Atom::Block {
				children: vec![],
			}
		]
	)
}