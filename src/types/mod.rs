//! Common types which are used across the compiler

#[derive(Debug, Clone, PartialEq)]
pub enum Item {
	Atom(Atom),
	Molecule(Molecule),
}

/// The `Atom` type is a syntax-level representation of the smallest possible code 
/// units, which have no semantic value directly associated with them.
#[derive(Debug, Clone, PartialEq)]
pub enum Atom {
	/// A collection of zero or more atoms, enclosed by round braces.
	List {
		children: Vec<Atom>,
	},

	/// A block introduces a new local scope and is delimited by curly braces.
	Block {
		children: Vec<Atom>,
	},

	/// A type identifier which starts with an uppercase letter, like `Lyn`
	Type {
		name: String,
	},

	Comment {
		content: String,
	},

	/// A variable name, which consists of one or more non-reserved Unicode 
	/// characters and an optional type annotation, like `x::Int` or `Δv`
	Symbol {
		name: String,
		type_id: Option<String>,
	},

	Literal(Literal),
}

#[derive(Debug, Clone, PartialEq)]
pub enum Molecule {
	Call {
		identifier: String,
		arguments: Vec<Molecule>,
	},

	Function {
		identifier: String,
		arguments: Vec<Molecule>,
		body: Box<Molecule>,
	},

	/// A structure which creates a new environment, and is enclosed by curly braces.
	Block {
		children: Vec<Molecule>,
	},

	/// A type identifier which starts with an uppercase letter, like `Lyn`
	Type {
		name: String,
	},

	/// A variable name, which consists of one or more non-reserved Unicode 
	/// characters and an optional type annotation, like `x::Int` or `Δv`
	Symbol {
		name: String,
		type_id: Option<String>,
	},

	Literal(Literal),
}

#[derive(Debug, Clone, PartialEq)]
pub enum Literal {
	/// A literal integer value, like `42`
	Integer {
		value: i64,
	},

	/// A literal floating-point value, like `3.141592`
	Float {
		value: f64,
	},

	/// A literal string value, like `"Hello, World!"`
	String {
		value: String,
	},

	/// A boolean value, which is either `true` or `false`
	Boolean {
		value: bool,
	},

	/// A literal list
	List {
		children: Vec<Item>,
	},
}